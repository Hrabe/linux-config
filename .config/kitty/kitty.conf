# vim:fileencoding=utf-8:ft=conf:foldmethod=marker

#: Fonts {{{

#: kitty has very powerful font management. You can configure
#: individual font faces and even specify special fonts for particular
#: characters.

font_family      Noto Sans Mono
bold_font        auto
italic_font      auto
bold_italic_font auto
font_size 16.0

#: }}}

#: Colors {{{

cursor #000000

foreground #000000
background #FBF1C7

# Black + DarkGrey
color0  #fdf4c1
color8  #928374
# DarkRed + Red
color1  #cc241d
color9  #9d0006
# DarkGreen + Green
color2  #98971a
color10 #79740e
# DarkYellow + Yellow
color3  #d79921
color11 #b57614
# DarkBlue + Blue
color4  #458588
color12 #076678
# DarkMagenta + Magenta
color5  #b16286
color13 #8f3f71
# DarkCyan + Cyan
color6  #689d6a
color14 #427b58
# LightGrey + White
color7  #7c6f64
color15 #3c3836

selection_foreground #bdae93
selection_background #3c3836 

#: }}}

# NO operation for default shortcuts

clear_all_shortcuts yes

# Font size 
map kitty_mod+plus  change_font_size all +2.0
map kitty_mod+minus change_font_size all -2.0
map kitty_mod+equal change_font_size all    0

# Copy
map kitty_mod+c  copy_to_clipboard
map kitty_mod+v  paste_from_clipboard
map kitty_mod+s  paste_from_selection

# Open links in default browser with double click
open_url_modifiers double
open_url_with $BROWSER

# Hide mouse cursor
focus_follows_mouse yes
mouse_hide_wait 2.0

