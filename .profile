export QT_QPA_PLATFORMTHEME="qt5ct"
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
export LANG=en_US.UTF-8

# Must specify fo i3-sensible-terminal to work 
export TERMINAL=/usr/bin/kitty
export EDITOR=/usr/bin/vim
export BROWSER=/usr/bin/chromium

# Local bin overrides
export PATH=$HOME/.local/bin:$PATH

export JAVA_HOME=/usr/bin/java
export JDK_HOME=/usr/lib/jvm/java-11-openjdk/bin/java
